package pkg

import (
	"fmt"
	"strings"
	"time"
)

func GeneratorNameFile(path string) string {
	now := time.Now().Unix()
	list := strings.Split(path, "/")
	return fmt.Sprintf("%d_%s", now, list[len(list)-1])
}
