package config

import (
	"github.com/ilyakaznacheev/cleanenv"
)

type (
	env struct {
		OpenAIDallE3ImageGenerationUrl string `env:"openai_dall_e_3_url_image_generations"`
		OpenAIApiKey                   string `env:"openai_sdk_api_key"`
		AppPort                        string `env:"APP_PORT" env-default:"3000"`
		AppHost                        string `env:"APP_HOST" env-default:"0.0.0.0"`
		AppApiKey                      string `env:"APP_API_KEY" env-required:"true"`
		DBMysqlSource                  string `env:"DB_MYSQL_SOURCE"`
		CloudflareR2APIUrl             string `env:"cloudflare_r2_api_url"`
		CloudflareR2ApiToken           string `env:"cloudflare_r2_api_token"`
		CloudflareAccountId            string `env:"cloudflare_account_id"`
	}
)

var Env env = env{}

func LoadEnv() error {
	return cleanenv.ReadConfig(".env", &Env)
}
