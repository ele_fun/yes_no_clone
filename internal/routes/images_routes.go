package routes

import (
	"net/http"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/handlers/images"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/middlewares/headers"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/middlewares/security"
)

func imagesRoutes() []router {
	return []router{
		{
			path:    "/image",
			methods: []string{http.MethodGet},
			middlewares: []middleware{
				headers.Middleware.ApplicationJson,
				headers.Middleware.CorsOrigin,
			},
			handlerFunc: images.Handler.Random,
		},
		{
			path:    "/image",
			methods: []string{http.MethodPost},
			middlewares: []middleware{
				security.Middleware.ApiKey,
				headers.Middleware.ApplicationJson,
				headers.Middleware.CorsOrigin,
			},
			handlerFunc: images.Handler.Create,
		},
		{
			path:    "/image/all",
			methods: []string{http.MethodGet},
			middlewares: []middleware{
				security.Middleware.ApiKey,
				headers.Middleware.ApplicationJson,
				headers.Middleware.CorsOrigin,
			},
			handlerFunc: images.Handler.All,
		},
		{
			path:    "/image/{id:[0-9]+}",
			methods: []string{http.MethodGet},
			middlewares: []middleware{
				security.Middleware.ApiKey,
				headers.Middleware.ApplicationJson,
				headers.Middleware.CorsOrigin,
			},
			handlerFunc: images.Handler.FindByID,
		},
	}
}
