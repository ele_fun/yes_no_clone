package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/handlers/storage"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/middlewares/headers"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/middlewares/security"
)

func storageRoutes() []router {
	prefix := "/storage"
	return []router{
		{
			path:    fmt.Sprintf("%s/upload", prefix),
			methods: []string{http.MethodPost},
			middlewares: []middleware{
				security.Middleware.ApiKey,
				headers.Middleware.CorsOrigin,
				headers.Middleware.ApplicationJson,
			},
			handlerFunc: storage.Handler.Upload,
		},
	}
}
