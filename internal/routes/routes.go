package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type (
	middleware func(http.HandlerFunc) http.HandlerFunc

	router struct {
		methods     []string
		handlerFunc http.HandlerFunc
		middlewares []middleware
		path        string
	}
)

func Routes() *mux.Router {
	r := mux.NewRouter()
	var list []router
	list = append(list, imagesRoutes()...)
	list = append(list, storageRoutes()...)

	for _, route := range list {
		r = handleFunc(r, route)
		log.Printf("%s - %s\n", route.methods, route.path)
	}

	return r
}

func handleFunc(r *mux.Router, route router) *mux.Router {

	for _, m := range route.middlewares {
		route.handlerFunc = m(route.handlerFunc)
	}
	r.HandleFunc(route.path, route.handlerFunc).Methods(route.methods...)

	return r
}
