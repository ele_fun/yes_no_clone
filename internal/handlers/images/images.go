package images

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	modelImage "gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/models/images"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/database"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/images"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage"
)

type (
	handler struct{}
)

var Handler handler = handler{}

type (
	CreateIn struct {
		Description *string `json:"description"`
	}
)

func (h *handler) Create(w http.ResponseWriter, r *http.Request) {
	// DONE: validar el input
	var description string = "frases motivacionales en español"
	var in CreateIn = CreateIn{Description: &description}
	t, err := io.ReadAll(r.Body)
	if err == nil {
		err = json.Unmarshal(t, &in)
		if err != nil || in.Description == nil {
			in.Description = &description
		}
	}

	log.Println("Description: ", *in.Description)

	// DONE: ejecutar el/los servicios
	result, err := images.Create(images.ImageIn{
		Description: *in.Description,
	})

	// DONE: responder
	if err != nil {
		e := fmt.Sprint(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(struct {
			Success bool `json:"success"`
			Error   any  `json:"error"`
		}{Success: false, Error: e})
		return
	}
	var url string = result.UrlImage

	// TODO: debe poder realizarse en segundo plano
	// TODO: guardar la imagen en un directorio
	// TODO: subir a un cdn la imagen obtenida
	body, err := storage.Download(result.UrlImage)
	//defer body.
	if err == nil {
		outUpload, err := storage.UploadByReader(body)
		log.Println(" --init upload by reader ----- ")
		log.Println(outUpload)
		log.Println(err)
		log.Println(" --end  upload by reader ----- ")
		if err == nil {
			url = outUpload.Url
		}
	}

	img, err := database.DB.Image.ImageNew(r.Context(), modelImage.Image{
		Url:         &url,
		Name:        &description,
		Description: &result.Description,
	})

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(*img)
	return
}

type (
	ResultAll struct {
		Success bool               `json:"success"`
		Size    int                `json:"size"`
		Items   []modelImage.Image `json:"items,omitempty"`
	}
)

func (h *handler) All(w http.ResponseWriter, r *http.Request) {

	all, err := database.DB.Image.ImageAll(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(struct {
			Success bool `json:"success"`
		}{Success: false})
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(ResultAll{
		Success: true,
		Size:    len(all),
		Items:   all,
	})
}

type (
	ResultFindById struct {
		Success bool             `json:"success"`
		Item    modelImage.Image `json:"item"`
	}
)

func (h *handler) FindByID(w http.ResponseWriter, r *http.Request) {

	idValue := mux.Vars(r)["id"]
	idTemp, err := strconv.Atoi(idValue)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(struct {
			Success bool `json:"success"`
		}{Success: false})
		return
	}

	id := int64(idTemp)

	img, err := database.DB.Image.ImageFindByID(r.Context(), modelImage.Image{ID: &id})
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(struct {
			Success bool `json:"success"`
		}{Success: false})
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(ResultFindById{
		Success: true,
		Item:    *img,
	})
}
func (h *handler) Random(w http.ResponseWriter, r *http.Request) {

	img, err := database.DB.Image.ImageRandom(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(struct {
			Success bool `json:"success"`
		}{Success: false})
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(ResultFindById{
		Success: true,
		Item:    *img,
	})
}
