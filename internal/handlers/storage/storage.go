package storage

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/models/images"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/database"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage"
)

type (
	handler struct{}
)

var Handler handler = handler{}

type (
	UploadIn struct {
		Path *string `json:"path"`
	}
	UploadOut struct {
		Success *bool `json:"success"`
	}
)

func (h *handler) Upload(w http.ResponseWriter, r *http.Request) {

	// DONE: validar el input
	var in UploadIn
	t, err := io.ReadAll(r.Body)
	if err == nil {
		err = json.Unmarshal(t, &in)
		if err != nil {
			w.WriteHeader(http.StatusConflict)
			json.NewEncoder(w).Encode(struct {
				Success bool `json:"success"`
				Error   any  `json:"error"`
			}{Success: false, Error: err})
			return
		}

		if in.Path == nil || *in.Path == "" {
			err = fmt.Errorf("path empty")
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(struct {
				Success bool `json:"success"`
				Error   any  `json:"error"`
			}{Success: false, Error: err})
			return
		}
	}

	log.Println("Path: ", *in.Path)

	// DONE: ejecutar el/los servicios
	resultUpload, err := storage.Upload(*in.Path)

	// DONE: insert on db
	image, err := database.DB.Image.ImageNew(r.Context(), images.Image{
		Name:        &resultUpload.Name,
		Description: &resultUpload.Description,
		Url:         &resultUpload.Url,
	})

	// DONE: responder
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]any{
		"item": image,
		"err":  err,
	})
	return
}
