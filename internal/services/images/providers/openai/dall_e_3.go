package openai

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/config"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/images"
)

type (
	DallE3 struct{}
)

type (
	responseDallE3 struct {
		Created int                  `json:"create"`
		Data    []dataResponseDallE3 `json:"data"`
	}
	dataResponseDallE3 struct {
		RevisedPrompt string `json:"revised_prompt"`
		Url           string `json:"url"`
	}
	input struct {
		Model  string `json:"model"`
		Prompt string `json:"prompt"`
		N      int    `json:"n"`
		Size   string `json:"size"`
	}
)

func NewDallE3ImageGenerator() *DallE3 {
	return &DallE3{}
}

func (d *DallE3) Create(in images.ImageIn) (out *images.ImageOut, err error) {
	out = &images.ImageOut{}
	header := http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("Authorization", fmt.Sprintf("Bearer %s", config.Env.OpenAIApiKey))

	bla, err := json.Marshal(input{
		Model:  "dall-e-3",
		Prompt: in.Description,
		N:      1,
		Size:   "1792x1024", // "1024x1024"
	})
	bodyRequest := bytes.NewBuffer(bla)

	req, err := http.NewRequest(http.MethodPost, config.Env.OpenAIDallE3ImageGenerationUrl, bodyRequest)
	req.Header = header

	var client http.Client

	response, err := client.Do(req)
	log.Println("response: ", response)
	log.Println("error: ", err)

	if err != nil {
		return nil, fmt.Errorf("code: %d, err: %v", response.StatusCode, err)
	}

	body, err := io.ReadAll(response.Body)
	log.Println("response.Body: ", string(body))
	defer response.Body.Close()

	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("code: %d, status: %v, body: %v", response.StatusCode, response.Status, string(body))
	}
	var data responseDallE3

	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}
	out.UrlImage = "http://empty.com"
	out.Description = "empty"

	if len(data.Data) > 0 {
		out.UrlImage = data.Data[0].Url
		out.Description = data.Data[0].RevisedPrompt
	}
	return
}
