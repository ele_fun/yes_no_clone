package images

type (
	Image interface {
		Create(in ImageIn) (out *ImageOut, err error)
	}
)

type (
	ImageOut struct {
		Description string `json:"description"`
		UrlImage    string `json:"url"`
	}
	ImageIn struct {
		Description string `json:"description"`
	}
)

var image Image

// SetImageGenerator - set interface image generator
func SetImageGenerator(i Image) {
	image = i
}

// Create - generación de la imagen por un servicio externo
func Create(in ImageIn) (*ImageOut, error) {
	return image.Create(in)
}
