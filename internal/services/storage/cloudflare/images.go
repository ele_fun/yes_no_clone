package cloudflare

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strings"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/config"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/pkg"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage"
)

type (
	Images struct {
		r2ApiUrl   string
		r2Apitoken string
		accountId  string
	}
)

var R2 Images = Images{}

func NewCloudflareR2() (*Images, error) {
	r2ApiUrl := config.Env.CloudflareR2APIUrl
	if r2ApiUrl == "" {
		return nil, fmt.Errorf("cloudflare_r2_api_url empty")
	}

	r2ApiToken := config.Env.CloudflareR2ApiToken
	if r2ApiToken == "" {
		return nil, fmt.Errorf("cloudflare_r2_api_token empty")
	}

	accountId := config.Env.CloudflareAccountId
	if accountId == "" {
		return nil, fmt.Errorf("cloudflare_account_id empty")
	}
	return &Images{
		r2ApiUrl:   r2ApiUrl,
		r2Apitoken: r2ApiToken,
		accountId:  accountId,
	}, nil
}

type (
	responseR2 struct {
		Success  bool             `json:"success"`
		Errors   []string         `json:"errors"`
		Messages []string         `json:"messages"`
		Result   responseR2result `json:"result"`
	}
	responseR2result struct {
		ID                string   `json:"id"`
		FileName          string   `json:"filename"`
		Uploaded          string   `json:"uploaded"`
		RequireSignedURLs bool     `json:"requireSignedURLs"`
		Variants          []string `json:"variants"`
	}
)

func (i *Images) Updload(pathImage string) (*storage.UploadOut, error) {
	// DONE: build url
	url := i.r2ApiUrl
	url = strings.ReplaceAll(url, ":accountId", i.accountId)

	// DONE: build request
	multipartOut, err := multipartFile(pathImage, pkg.GeneratorNameFile(pathImage))
	if err != nil {
		return nil, fmt.Errorf("multipart file, error: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, multipartOut.b)
	if err != nil {
		return nil, fmt.Errorf("build request, error: %v", err)
	}

	request.Header.Set("Content-Type", multipartOut.contentType)
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", i.r2Apitoken))

	// DONE: build client http
	var client http.Client
	// DONE: call
	result, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("call api error: %v", err)
	}
	defer result.Body.Close()

	// DONE: logic
	body, err := io.ReadAll(result.Body)
	if err != nil {
		return nil, err
	}

	if result.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("StatusCode: %d, status: %s", result.StatusCode, result.Status)
	}
	var r responseR2
	err = json.Unmarshal(body, &r)
	if err != nil {
		return nil, err
	}

	// DONE: response
	return &storage.UploadOut{
		Name:        r.Result.FileName,
		Url:         r.Result.Variants[0],
		Description: r.Result.ID,
	}, nil
}
func (i *Images) UploadByReader(object *storage.ObjectFileOut) (*storage.UploadOut, error) {

	url := i.r2ApiUrl
	url = strings.ReplaceAll(url, ":accountId", i.accountId)
	request, err := http.NewRequest(http.MethodPost, url, object.Reader)
	if err != nil {
		return nil, fmt.Errorf("build request, error: %v", err)
	}

	request.Header.Set("Content-Type", object.ContentType)
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", i.r2Apitoken))

	// DONE: build client http
	var client http.Client

	// DONE: call
	result, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("call api error: %v, result: %v", err, result)
	}
	defer result.Body.Close()

	// DONE: logic
	body, err := io.ReadAll(result.Body)
	if err != nil {
		return nil, err
	}

	if result.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("StatusCode: %d, status: %s,\nbody: %s", result.StatusCode, result.Status, string(body))
	}
	var r responseR2
	err = json.Unmarshal(body, &r)
	if err != nil {
		return nil, err
	}

	// DONE: response
	return &storage.UploadOut{
		Name:        r.Result.FileName,
		Url:         r.Result.Variants[0],
		Description: r.Result.ID,
	}, nil
}

type (
	multipartFileOut struct {
		b           *bytes.Buffer
		contentType string
	}
)

func multipartFile(pathFile, nameFile string) (*multipartFileOut, error) {
	var out multipartFileOut
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	// open file
	f, err := os.Open(pathFile)
	if err != nil {
		return nil, fmt.Errorf("Error al abrir el fichero: %v", err)
	}
	defer f.Close()

	// Crear un formulario con un campo del fichero
	fw, err := w.CreateFormFile("file", nameFile)
	if err != nil {
		return nil, fmt.Errorf("Error al crear el formulario :%v", err)
	}
	// Copiar el contenido del fichero al formulario
	_, err = io.Copy(fw, f)
	if err != nil {
		return nil, fmt.Errorf("Error al copiar el contenido del fichero al formulario: %v", err)
	}
	defer w.Close()

	out.b = &b
	out.contentType = w.FormDataContentType()

	return &out, nil
}
