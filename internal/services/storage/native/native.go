package native

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/pkg"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage"
)

type (
	downloadNative struct{}
)

func NewDownloadNative() *downloadNative {
	return &downloadNative{}
}

func (d *downloadNative) Download(url string) (*storage.ObjectFileOut, error) {

	nameFile := pkg.GeneratorNameFile("/native")
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Error al realizar la solicitud HTTP: %v", err)
	}
	defer resp.Body.Close()

	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fw, err := w.CreateFormFile("file", nameFile)
	if err != nil {
		return nil, fmt.Errorf("Error al crear el formulario :%v", err)
	}
	_, err = io.Copy(fw, resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Error al copiar la imagen al formulario, error: %v", err)
	}
	defer w.Close()

	var out storage.ObjectFileOut

	out.Reader = &b
	out.ContentType = w.FormDataContentType()

	return &out, nil
}
