package storage

import "io"

type (
	Object interface {
		// Updload - upload object
		Updload(pathImage string) (*UploadOut, error)
		UploadByReader(*ObjectFileOut) (*UploadOut, error)
	}
	UploadOut struct {
		Name        string
		Url         string
		Description string
	}
)

var object Object

func Set(o Object) {
	object = o
}

func Upload(path string) (*UploadOut, error) {
	return object.Updload(path)
}

func UploadByReader(o *ObjectFileOut) (*UploadOut, error) {
	return object.UploadByReader(o)
}

type (
	ObjectFile interface {
		Download(url string) (*ObjectFileOut, error)
	}
	ObjectFileOut struct {
		Reader      io.Reader
		ContentType string
	}
)

var objectFile ObjectFile

func SetObjectFile(o ObjectFile) {
	objectFile = o
}

func Download(url string) (*ObjectFileOut, error) {
	return objectFile.Download(url)
}
