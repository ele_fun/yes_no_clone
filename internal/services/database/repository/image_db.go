package repository

import (
	"context"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/models/images"
)

type (
	ImageDB interface {
		ImageAll(ctx context.Context) ([]images.Image, error)
		ImageFindByID(ctx context.Context, i images.Image) (*images.Image, error)
		ImageNew(ctx context.Context, i images.Image) (*images.Image, error)
		ImageRandom(ctx context.Context) (*images.Image, error)
	}
)
