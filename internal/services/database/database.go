package database

import "gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/database/repository"

type (
	Database struct {
		Image repository.ImageDB
	}
)

var DB Database

func Set(d Database) {
	DB = d
}
