package mysql

import (
	"context"
	"fmt"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/models/images"
)

func (m *Mysql) ImageAll(ctx context.Context) (list []images.Image, err error) {

	rows, err := m.db.QueryContext(ctx, "Select id, name, url, description from AI.images")
	defer rows.Close()

	if err != nil {
		list = nil
		return
	}
	if rows.Err() != nil {
		list, err = nil, rows.Err()
		return
	}

	list = []images.Image{}
	for rows.Next() {
		var img images.Image
		err = rows.Scan(&img.ID, &img.Name, &img.Url, &img.Description)
		if err != nil {
			list = nil
			return
		}
		list = append(list, img)
	}

	return
}
func (m *Mysql) ImageFindByID(ctx context.Context, img images.Image) (image *images.Image, err error) {
	query := fmt.Sprintf("Select id, name, url, description From AI.images Where id = ? Limit 1")
	row := m.db.QueryRowContext(ctx, query, img.ID)

	err = row.Err()
	if err != nil {
		image = nil
		return
	}
	image = &images.Image{}

	err = row.Scan(&image.ID, &image.Name, &image.Url, &image.Description)
	if err != nil {
		image = nil
		return
	}

	return
}
func (m *Mysql) ImageNew(ctx context.Context, img images.Image) (image *images.Image, err error) {
	script := fmt.Sprintf("Insert Into AI.images(name, url, description) Values (?, ?, ?) ")
	result, err := m.db.ExecContext(ctx, script, img.Name, img.Url, img.Description)
	if err != nil {
		image = nil
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		image = nil
		return
	}

	img.ID = &id
	image, err = m.ImageFindByID(ctx, img)
	if err != nil {
		image = nil
		return
	}

	return
}
func (m *Mysql) ImageRandom(ctx context.Context) (image *images.Image, err error) {
	query := fmt.Sprintf("Select id, name, url, description From AI.images Order By RAND() Limit 1;")
	result := m.db.QueryRowContext(ctx, query)
	if err = result.Err(); err != nil {
		return nil, err
	}
	var img images.Image

	if err = result.Scan(&img.ID, &img.Name, &img.Url, &img.Description); err != nil {
		return nil, err
	}
	image = &img

	return
}
