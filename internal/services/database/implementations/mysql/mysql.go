package mysql

import (
	"database/sql"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/config"
)

type (
	Mysql struct {
		db *sql.DB
	}
)

func NewMysqlDB() *Mysql {
	source := config.Env.DBMysqlSource
	if source == "" {
		log.Fatalln("error, mysql datasource empty")
	}
	db, err := sql.Open("mysql", source)
	if err != nil {
		log.Fatalln("error, conection mysql, err: ", err)
	}
	time.Sleep(3 * time.Second)
	if err = db.Ping(); err != nil {
		log.Fatalln("--- no connection db, error: ", err)
	}
	log.Println(" ------ connection success ------ ")

	return &Mysql{
		db: db,
	}
}
