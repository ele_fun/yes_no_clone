package images

type (
	Image struct {
		ID          *int64  `json:"id"`
		Name        *string `json:"name"`
		Url         *string `json:"url"`
		Description *string `json:"description"`
	}
)
