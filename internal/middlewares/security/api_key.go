package security

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/config"
)

func (m *middleware) ApiKey(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// DONE: captura del api_key del header
		apiKey := r.Header.Get("api_key")

		// DONE: validación del api_key
		if apiKey == config.Env.AppApiKey {
			next.ServeHTTP(w, r)
			return
		}

		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(struct {
			Success bool `json:"success"`
		}{Success: false})
		return
	}
}
