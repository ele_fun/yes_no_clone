package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/config"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/routes"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/database"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/database/implementations/mysql"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/images"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/images/providers/openai"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage/cloudflare"
	"gitlab.com/Elevarup/ele_fun/yes_no_clone/internal/services/storage/native"
)

func main() {
	if err := config.LoadEnv(); err != nil {
		log.Println("environment, error: ", err)
		os.Exit(0)
	}
	// DONE: setear el generador de imágenes
	d := openai.NewDallE3ImageGenerator()
	images.SetImageGenerator(d)

	// DONE: inicializar el servidor
	server := http.Server{
		Addr:    fmt.Sprintf("%s:%s", config.Env.AppHost, config.Env.AppPort),
		Handler: routes.Routes(),
	}

	// DONE: init database
	mysqlDB := mysql.NewMysqlDB()
	database.Set(database.Database{
		Image: mysqlDB,
	})

	// DONE: init storage
	r2, err := cloudflare.NewCloudflareR2()
	if err != nil {
		log.Println(err)
		os.Exit(0)
	}
	storage.Set(r2)
	// DONE: init store ObjectFile
	nat := native.NewDownloadNative()
	storage.SetObjectFile(nat)

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Println("listen and server, error: ", err)
			os.Exit(0)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	server.Shutdown(ctx)
	log.Println("shutdown server")
}
